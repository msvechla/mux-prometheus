# mux-prometheus

[![pipeline status](https://gitlab.com/msvechla/mux-prometheus/badges/master/pipeline.svg)](https://gitlab.com/msvechla/mux-prometheus/commits/master) [![coverage report](https://gitlab.com/msvechla/mux-prometheus/badges/master/coverage.svg)](https://gitlab.com/msvechla/mux-prometheus/commits/master) [![go report](https://goreportcard.com/badge/gitlab.com/msvechla/mux-prometheus)](https://goreportcard.com/report/gitlab.com/msvechla/mux-prometheus) [![go.dev reference](https://img.shields.io/badge/go.dev-reference-007d9c?logo=go&logoColor=white)](https://pkg.go.dev/gitlab.com/msvechla/mux-prometheus@v0.0.1/pkg/middleware?tab=doc)

*mux-prometheus is a prometheus metrics exporter implemented as middleware for the powerful [gorilla/mux](https://github.com/gorilla/mux) http router.*

<!-- TOC -->

- [Collected Metrics](#collected-metrics)
- [Labels](#labels)
- [Getting Started](#getting-started)
- [Customization Options](#customization-options)
    - [Route Template vs Actual Route Labels](#route-template-vs-actual-route-labels)
    - [Request Duration Buckets](#request-duration-buckets)
- [Request Size Estimation](#request-size-estimation)
- [Contributing](#contributing)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

<!-- /TOC -->

## Collected Metrics

The following metrics are collected by mux-prometheus:

| metric                                           | type      | description                           |
|--------------------------------------------------|-----------|---------------------------------------|
| `$namespace_$subsystem_requests_total`           | counter   | The total number of requests received |
| `$namespace_$subsystem_request_size_bytes`       | summary   | Summary of request bytes received     |
| `$namespace_$subsystem_request_duration_seconds` | histogram | Histogram of the request duration     |
| `$namespace_$subsystem_response_size_bytes`      | summary   | Summary of response bytes sent        |

Both the `$namespace` and `$subsystem` parameters are configurable. They default to `mux` and `router` respectively.

## Labels

By default the following labels will be captured with every metric:

- `code` - the http status code
- `method` - the http method of the request
- `host` - the hostname specified in the request
- `route` - the route template matched or raw route used in the request

## Getting Started

Getting started is as simple as using the default mux-prometheus middleware:

```go
package main

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	muxprom "gitlab.com/msvechla/mux-prometheus/pkg/middleware"
)

func main() {
	r := mux.NewRouter()

	instrumentation := muxprom.NewDefaultInstrumentation()
	r.Use(instrumentation.Middleware)

	r.Path("/metrics").Handler(promhttp.Handler())
	r.Path("/helloworld/{id}").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {})

	srv := &http.Server{Addr: "localhost:8080", Handler: r}
	srv.ListenAndServe()
}
```

This will initialize mux-prometheus with sane defaults and serve metrics on the `/metrics` endpoint.

## Customization Options

A mux-prometheus can be initialized with custom options via the `NewCustomInstrumentation()` function. The full set of customization options can be found in the [go.dev reference](https://pkg.go.dev/gitlab.com/msvechla/mux-prometheus@v0.0.1/pkg/middleware?tab=doc).

### Route Template vs Actual Route Labels

Prometheus [recommends using low cardinality for all labels](https://prometheus.io/docs/practices/naming/#labels), as every unique combination of key-value label pairs generates a new time series in the database.

Therefor mux-prometheus will by default use mux route templates as labels to annotate the metrics. In other words a request to `/helloworld/test` and `/helloworld/yay` will both be labeled with `route: /helloworld/{id}` in case such a template is defined.

In case this behavior is not desired, the option `UseRouteTemplate` can be specified on the custom instrumentation.

### Request Duration Buckets

The buckets of the request duration summary can be customized via the `ReqDurationBuckets` parameter of the custom instrumentation. By default the prometheus defaults for buckets will be used.

## Request Size Estimation

Currently the request size is estimated on a best-effort basis. The actual size of the body is not calculated at the moment and the implementation relies on the `Content-Length` header being set and some additional logic for estimating the metadata. The reason for this estimation is mainly performance optimization, as we would have to read the entire body and afterwards making it re-readable again from the `io.Reader` interface.

The current implementation can be found [here](https://gitlab.com/msvechla/mux-prometheus/-/blob/master/pkg/middleware/middleware.go#L131).

We might add an option to calculate the actual body size in the future if there is interest in this.

## Contributing

Please read [CONTRIBUTING.md]() for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/msvechla/mux-prometheus/tags) or take a look at the [CHANGELOG.md](./CHANGELOG.md)

## Authors

- **Marius Svechla** - *Initial work*

See also the list of [contributors](https://gitlab.com/msvechla/mux-prometheus/graphs/master) who participated in this project.

## License

[MIT License](./LICENSE.md)  
Copyright (c) [2020] [Marius Svechla]

## Acknowledgments

- Huge thanks to the awesome [gorilla/mux](https://github.com/gorilla/mux) router project ❤️
