module gitlab.com/msvechla/mux-prometheus

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.8.0
	gotest.tools v2.2.0+incompatible
	gotest.tools/v3 v3.0.3
)
